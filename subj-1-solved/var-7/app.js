function applyDiscount(vehicles, discount){
    return new Promise((resolve, reject) => {
        if(typeof discount !== "number"){
            reject(new Error("Invalid discount"));
        }
        else{
            var ok = 1;
            vehicles.forEach(vech =>{
                if(typeof vech.make !== "string" || typeof vech.price !== "number"){
                    ok=0;
                }
            })
            if(ok==0){
                reject(new Error("Invalid array format"));
            }
            else{
                var min = vehicles[0].price;
                for(let i=1; i<vehicles.length; i++){
                    if(vehicles[i].price<min){
                        min = vehicles[i].price;
                    }
                }
                if(discount > 0.5*min){
                    reject("Discount too big");
                }
                else{
                    var newArray = [];
                    for(let i=0; i<vehicles.length; i++){
                        var obj = new Object();
                        obj.make = vehicles[i].make;
                        obj.price = vehicles[i].price - discount;
                        newArray.push(obj);
                    }
                    resolve(newArray);
                }
            }
        }
    })
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;